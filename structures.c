#include <stdio.h>

struct xample {
	int x;
};

int main()
{
	struct xample structure;
	struct xample *ptr;

	structure.x = 12;
	ptr = &structure; /* you need the & when dealing with structures and 
						using pointers to them */

	printf("%d\n", ptr->x); /* The -> acts somewhat like the * when it is
							used with pointers it says, get whatever is at that memory address not get what the memory address is */										
	getchar();
}


