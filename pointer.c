#include <stdio.h>

int main()
{
	int x;   /* normal integer */
	int *p;  /* a pointer to an integer */

	p = &x;
	scanf("%d", &x);
	printf("%d\n", *p);
	getchar();
}

