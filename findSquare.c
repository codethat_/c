#include <stdio.h>

/* Takes 10 integers as input, prints out the square of each one except element 4 */

int main() {
	int i; 
	int x[10];
	printf("Enter number: ");
	scanf("%d%d%d%d%d%d%d%n%d%d\n", &x[0], &x[1], &x[2], &x[3], &x[4], &x[5], &x[6], &x[7], &x[8], &x[9]);
	printf("%d%d%d%d%d%d%d%d%d%d\n", x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9]);

	return 0;
}

