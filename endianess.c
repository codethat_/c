#include <stdio.h>
#include <endian.h>

int main (void)
{
#if BYTE_ORDER == LITTLE_ENDIAN
    printf("system is little Endian \n");
#elif BYTE_ORDER == BIG_ENDIAN
    printf("system is big Endian \n");
#else
    printf("whats going on? \n");
#endif

    return 0;
}
