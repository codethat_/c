#include <stdio.h>
#include <math.h>

int main()
{
	int num1;
	int num2;
	printf("Enter 2 numbers:");
	scanf("%d%d", &num1, &num2);

	if(num1 == num2) {
		printf("%d are equal %d");
	}
	if(num1 > num2) {
		printf("%d is larger than %d", num1, num2);
	}
	if(num1 < num2) { 
		printf("%d is less than %d", num1, num2);
	}

	getchar();

	return 0;
}
